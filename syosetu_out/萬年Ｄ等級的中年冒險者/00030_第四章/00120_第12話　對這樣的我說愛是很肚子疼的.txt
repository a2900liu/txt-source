寬敞的屋子裡有很多客房，我們各自都選擇了別的房間。

話雖如此，我現在和琉娜在同一張床上。
不用說，今天晚上該愛她了。
一瞬間腦海中浮現出「在別人家的床上幹怎麼樣」的想法，但在慾望面前卻被一掃而光。

⋯⋯昨晚喝醉了。

順帶一提，酒味兒撲鼻而來，回到屋子裡，被塞蕾斯怒斥了一頓。

姑且不論那個。

度過互相尋求的熱情濃厚的一晚，那個第二天早上。

一睜開眼睛，精靈的美貌就佔據了視野。
嘴唇有柔軟的感觸。

「嗯⋯⋯你醒了嗎，盧卡斯大人」

注意到我醒來了，琉娜稍微拉開了臉。
她總是先醒來，隨意親吻還在睡覺的我。

「你睡得還是那麼可愛。所以不由得想親吻他」
「不，怎麼想都覺得大叔的睡臉很可愛吧。琉娜更可愛」
「沒有那樣的事」
「問一百個人，一百個人會同意我的吧。」
「至少，對我來說是不同的」

說著，琉娜再次把嘴唇緊緊地貼了過來。
我一邊接受那個，一邊撫摸她的頭，手勢一邊梳艷麗的頭髮。
醒來之後，在床上玩耍也是最近的流行（？）

但是那個時候。

「差不多該起床了。準備了早餐──」

門擅自開了。

哎呀⋯⋯總覺得似曾相識⋯⋯⋯

而且人化了的維納斯在門的旁邊，浮現著深深地毒辣的笑容。

「──什麼⋯⋯什⋯⋯什⋯⋯」

走進房間的塞蕾斯蒂妮看見我和琉娜重疊在一起。

眼看著臉通紅，

「你們在幹什麼啊！？」

悲鳴的叫聲響了起來。

「可惡，你這麼慌張怎麼了？」

維納斯微笑著問為了不看這邊而轉過身來的塞蕾斯蒂妮。

「肯、肯定的吧！？因為她在人家做著令人討厭的事情⋯⋯啊！」
「嗚呼呼？嗚呼呼？那是什麼令人討厭的事？你到底為什麼這麼討厭，不告訴我嗎？正如我所見，我還是個孩子。不知道兩個人在幹什麼」

對呼喊的塞雷斯蒂寧，維納斯提出了更加壊的問題。

「那、那、那、那、那個不可能隨便說的吧！？」
「不能隨便說的話？難道說，實際上你也不明白這行為的意思嗎？明明不知道在幹什麼，卻憤慨地否定。」
「哇哇，我知道啦！啊，那是⋯⋯那、那個⋯⋯嗚⋯⋯」
「怎麼了？不能說嗎？果然還是不能理解啊。」
「所以我很清楚！那、那個⋯⋯せ、せ、せ、せ⋯⋯」
「親？親？聽得不清楚啊？用更大的聲音清楚的說──嘟嘟！？」

我扔的枕頭直擊了維納斯的臉。
你適可而止吧。

「嗚⋯⋯我做事很疏忽。事前應該好好地禁止的⋯⋯」

懷恨感嘆的塞蕾斯蒂妮。

「對塞雷斯隊長說什麼無禮⋯⋯！」
「雖然有我們⋯⋯疏忽⋯⋯啊！」

她的部下的聖騎士們，以非常厲害的形式注視著這邊。

「傻瓜！這是為什麼呢！」

反駁的是維納斯。
不知不覺又擅自人化了。

⋯⋯大概是害怕看守犬的吉魯吧，東張西望四周。

不愧是那麼大，不會在室內的。

「維納斯，你給我沉默一下。⋯⋯的確，在人家家裡不應該做這樣的事情啊。」

我也覺得很抱歉。
路上的旅館也看得很普通⋯⋯那是生意，既然承認男女可以住在一個房間裡，就應該事先了解了吧。
雖然在這裡特意給我們寄了包間，但還是移動了房間。

確實被責備也是沒辦法的事。

「打掃衛生，要自己好好打掃」
「啊，這樣的話，我來做！」

在那裡插嘴了的，是這次旅行附有了的自稱・女僕長的伊蕾拉。
她討厭被當做客人看待，自己買通了這個房子的女僕出去了。

「哎呀，真不能讓你做到這種地步。」

畢竟是體液性的東西被弄髒了⋯⋯⋯

「不不，因為清洗主人和夫人的床是女僕的職責！斯哈哈」
「⋯⋯為什麼鼻息這麼大呢？」

我覺得絶對不能交給她。

「總之！不要再做那種事情了！至少在這座城市，在審問結束前嚴禁入內！」
「那可不好辦。因為那是必不可少的事情。」

塞蕾斯蒂妮用強硬的語調來控訴，但是卻遭到了琉娜的反駁。

「和盧卡斯殿好幾天都做不到，現在的我無法忍受。」
「我也是」
「我、我也是⋯⋯」

艾莉婭和珂露雪也同意。

⋯⋯忍耐幾天吧？

「沒辦法。在宅邸內做很難的話，在外面做就可以了」
「知道了，夫人！那麼準備帳篷！還帶著這樣的東西呢！」

不，只是設想萬一的時候，收藏在珂露雪的影子裡吧。

「因為不是這這個問題！原本婚姻前的男女⋯⋯那個⋯⋯那，做這種事，是不能有的！」

加之，塞蕾斯蒂妮繼續說道。

「像這樣圍繞著幾個人，真是豈有此理！能夠被被原諒的只是一對一的夫妻關係！」

正如隊長所說的那樣！這個淫亂惡魔！等，部下的聖騎士們強有力地表示同意。

「哈哈，小姑娘對我說愛，真是肚子疼啊。」

象小鹿一樣地發出鼻子的是維納斯。

「喂喂，反正那個年紀還沒生過女兒吧？只是愚蠢地囫圇吞棗地相信某人的話，連和異性相愛的經驗都沒有。」
「當然！我是侍奉神明的聖職者！我決定一生都堅持獨身！」
「什麼？你打算連結婚都不結婚嗎？啊，連性的喜悅也不知道就死了，多可憐啊？」

我抓住維納斯的脖子使之沉默「別吵架」

從兩個人的主張來看，無論爭論到什麼地步，也只能是平行線吧。

「啊，你們這樣做可以嗎？對自己有好感的男性竟然和自己以外的女性保持著關係，這怎麼能原諒呢！」

塞蕾斯蒂妮把矛頭指向了艾莉婭她們，

「沒有那樣的事」
「嗯」
「我也不介意。倒不如說像盧卡斯殿那樣的男人，圍著很多女性是理所當然的。」

被一腳踢開。

仿彿失去了語言般愕然的她，

「⋯⋯啊，女神⋯⋯請一定要正確地引導這些人⋯⋯」

開始向女神祈禱了。

⋯⋯是一種逃避現實吧。